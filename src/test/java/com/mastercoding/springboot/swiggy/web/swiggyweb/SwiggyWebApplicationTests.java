package com.mastercoding.springboot.swiggy.web.swiggyweb;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SwiggyWebApplicationTests {

	@Test
	public void contextLoads() {
	}

}
