package com.mastercoding.springboot.swiggy.web.swiggyweb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@SpringBootApplication
public class SwiggyWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(SwiggyWebApplication.class, args);
		new SwiggyWebApplication().getData();

	}

	public void getData() {
		RestTemplate restTemplate = new RestTemplate();
		Restaurant restaurant = restTemplate
				.getForObject("http://localhost:9090/restaurants?name=swagat", Restaurant.class);
		System.out.println(restaurant);

		Restaurant newRestaurant = new Restaurant();
		newRestaurant.setArea("Madhapur");
		newRestaurant.setName("Barbeque Nation");
		newRestaurant.setOpenTime("10:00");
		newRestaurant.setCloseTime("22:00");
		HttpEntity<Restaurant> request = new HttpEntity<>(newRestaurant);
		ResponseEntity<Integer> response = restTemplate
				.exchange("http://localhost:9090/restaurants", HttpMethod.PUT, request, Integer.class);

		System.out.println(response.getStatusCode());

		ResponseEntity<List<Restaurant>> restaurants = restTemplate
				.exchange("http://localhost:9090/restaurants1", HttpMethod.GET, null, new ParameterizedTypeReference<List<Restaurant>>(){});
		System.out.println(restaurants.getStatusCode());
		System.out.println(restaurants.getBody());

	}
}
