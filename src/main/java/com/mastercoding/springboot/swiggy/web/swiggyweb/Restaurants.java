package com.mastercoding.springboot.swiggy.web.swiggyweb;

import java.util.List;

public class Restaurants {
    private List<Restaurant> restaurantList;

    public List<Restaurant> getRestaurantList() {
        return restaurantList;
    }

    public void setRestaurantList(List<Restaurant> restaurantList) {
        this.restaurantList = restaurantList;
    }
}
